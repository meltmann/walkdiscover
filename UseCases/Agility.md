#  Use Cases

## An Approach In Agility

----
# Automagically created

- by Marco Feltmann
- at 2020-12-02T22:41:33Z

You might ask the authoring authority for an update if needed.

----

# Backlog

----

#  [C2] GPX support

Besides the current location and activity tracks we want some more inputs.
More inputs mean more tools and devices our app can interact with.
Import of [GPX](https://en.wikipedia.org/wiki/GPS_Exchange_Format) would be nice.

----

#  [C2] GeoJSON support

Besides the current location and activity tracks we want some more inputs.
More inputs mean more tools and devices our app can interact with.
Import of [GeoJSON](https://geojson.org) would be nice.

----

#  [S3] Import tracks from Activities

Besides the current location stored tracks need to be drawn, too.
We need to import tracks from Activities

----

# Options

----

#  [M2] Use Current Location

In a first test we'd like to compare the fog with our current location.
So we need to get and display our current location.

----

#  [M8] Fog of War

To display Fog of War on a map we need the fog.
An approach has to be found to render that fog.
Unfortunately taking a black overlay and removing some portions doesn't work.

----

#  [M3] Implement MapKit

To see some Fog of War on a map we need a map.
MapKit seems reasonable.
But maybe we need to switch to or support Google Map as well!

----

# That's All Folks.
## In Hamburg sagt man "Tschüss"!
