#  [M8] Fog of War

To display Fog of War on a map we need the fog.
An approach has to be found to render that fog.
Unfortunately taking a black overlay and removing some portions doesn't work.
