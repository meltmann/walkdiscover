#  [C2] GPX support

Besides the current location and activity tracks we want some more inputs.
More inputs mean more tools and devices our app can interact with.
Import of [GPX](https://en.wikipedia.org/wiki/GPS_Exchange_Format) would be nice.
