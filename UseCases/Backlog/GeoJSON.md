#  [C2] GeoJSON support

Besides the current location and activity tracks we want some more inputs.
More inputs mean more tools and devices our app can interact with.
Import of [GeoJSON](https://geojson.org) would be nice.
