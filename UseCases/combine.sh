#!/bin/bash

### You may provide a better output file default name if you need to.
outputFileName=Agility.md
    
### Output would be written to a given output file.
if test "$# -ge 1"; then
    outputFileName=$1
    echo "Output to $outputFileName"
fi

scriptRoot="$SRCROOT/../UseCases"

function outputPageBreak {
    echo "" >> $outputFileName
    echo "----" >> $outputFileName
    echo "" >> $outputFileName
}

function outputPresentationHeading {
    cat "$scriptRoot/meta/Heading.md" > $outputFileName

    echo "# Automagically created" >> $outputFileName
    echo "" >> $outputFileName
    echo -n "- by " >> $outputFileName
    dscl . -read /Users/`whoami` RealName | sed -n 's/^ //g;2p' >> $outputFileName
    echo -n "- at " >> $outputFileName
    date -u +"%FT%TZ" >> $outputFileName
    echo "" >> $outputFileName
    echo "You might ask the authoring authority for an update if needed." >> $outputFileName
    
    outputPageBreak
}

function outputPresentationFooter {
    echo "# That's All Folks." >> $outputFileName
    echo "## In Hamburg sagt man \"Tschüss\"!" >> $outputFileName
}

function outputUseCases {
### If you like to change the directory structure make sure to reflect your changes in the referenced directory array below as well.
    directories=( "$scriptRoot/Backlog" "$scriptRoot/Options" "$scriptRoot/InProgress" "$scriptRoot/InTesting" "$scriptRoot/Done" )
    
    for currentDirectory in "${directories[@]}"; do
        if test "$(ls -A $currentDirectory)"; then
            echo "# `basename $currentDirectory`" >> $outputFileName;

            outputPageBreak

            for file in `ls ${currentDirectory}/*.md`; do
                cat "${file}" >> $outputFileName;
            
                outputPageBreak
            done
        fi
    done
}

echo "Combining sub documents"

## Main Script Entry Point
#

outputPresentationHeading

outputUseCases

outputPresentationFooter
