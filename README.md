# WalkDiscover

An approach on `SwiftUI` and `MapKit` to keep track of your journeys in a "fog-of-war" like manner.

## Why is this?

For fun.
Well, actually, to be completely honest, it is due to COVID-19.
Someone™ forced me to "go for a walk once a day". Unfortunately I'm not that [Ingress](https://www.ingress.com) / [Pokemon Go](https://pokemongolive.com/de/) / [Jurassic World Alive](https://www.jurassicworldalive.com) / [Wizards Unite](https://www.harrypotterwizardsunite.com) kinda go-out-and-play type.

I'm a coder, right? And I'm way too old for that fancy AR-whatever stuff.
Did I mention the annoying "spend real money for non-existing stuff you don't really need but you cannot master this chillingly without doing so" attitude of those games?

## What is this?

I want to implement some [Fog Of War](https://en.wikipedia.org/wiki/Fog_of_war).

This way it is much easier to find out where you've been walking the last time and how long this last time may be ago. The darker the shroud, the longer ago and completely black means [*here be serpents, dragons and basilisks*](https://en.wikipedia.org/wiki/Here_be_dragons) (which, in fact, shouldn't really be there, though).

Since I'm a coder (guess I mentioned it some time before) my aim is *not* to build the next-level highly-interactive immersive-AR outdoor map application and selling "non-existing stuff you don't really need but you cannot master this chillingly without doing so". My aim is the coding itself.
Boring, you might say.

Like some post(wo)man who throws away her/his backpack of 20kg dead trees after walking through the city for about 8 hours getting the mail delivered, just to put on her/his hiking boots and go on a wood trail for about 8 hours with 20kg of water, food and other luggage in her/his backpack.

There may be post(wo)men doing that, but I don't know any of them.
In fact I do know a lot of coderiñas that code in their spare time so they can code cool things they cannot code when they code for a living.

One inspiring coder for this coding who code the code for fun as well as for a living is the awesome [Dominik Hauser PhD](https://dasdom.dev) with his project [Walk2Draw](https://github.com/dasdom/Move2Draw) created for his book [Build Location-Based Projects for iOS](https://pragprog.com/titles/dhios/build-location-based-projects-for-ios/)
