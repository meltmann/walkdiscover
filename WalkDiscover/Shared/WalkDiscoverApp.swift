//
//  WalkDiscoverApp.swift
//  Shared
//
//  Created by Marco Feltmann on 02.12.20.
//

import SwiftUI

@main
struct WalkDiscoverApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
